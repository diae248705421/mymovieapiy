from config.database import Base
from sqlalchemy import Column, Integer, String, Float

class Movie(Base):
  __tablename__ = "movies"

  id = Column(Integer, primary_key = True)
  title = Column(String)
  overview = Column(String)
  year = Column(Integer)
  rating = Column(Float)
  category = Column(String)

#nuevo
class Pc(Base):
  __tablename__ = "pc"

  id = Column(Integer, primary_key = True)
  marca = Column(String)
  modelo = Column(String)
  color = Column(String)
  RAM = Column(String)
  almacenamiento = Column(String)