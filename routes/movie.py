from fastapi import APIRouter
from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse

from pydantic import BaseModel, Field
from typing import  Optional, List

from config.database import Session
from models.movie import Movie as MovieModel
#nuevo
from models.movie import Pc as PcModel

from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer

from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()
#nuevo
pc_router = APIRouter()

class Pc(BaseModel):
    id: Optional[int] = None 
    marca: str = Field(min_length=1, max_length=15)
    modelo: str = Field(min_length=1, max_length=50)
    color: str = Field(min_length=1, max_length=50)
    RAM: str = Field(min_length=1, max_length=50)
    almacenamiento: str = Field(min_length=1, max_length=15)

@movie_router.get("/movies", tags=["movies"], response_model = List[Movie],status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
  db = Session()
  result = MovieService(db).get_movies()
  return JSONResponse(content=jsonable_encoder(result),status_code=200)


@movie_router.get("/movies:{id}",tags=["movies"])
def get_movie(id : int = Path(ge=1,le=2000)) -> Movie:
  db = Session()
  result = MovieService(db).get_movie(id)
  #  for item in movies:
  #   if item["id"] == id:
  #     return JSONResponse(content=item)
  if not result:
    return JSONResponse(status_code=404, content={'message': 'No encontrado'}) 
  return JSONResponse(content=jsonable_encoder(result),status_code=200)

@movie_router.get('/movies/', tags=['movies'])
def get_movies_by_category(category:str = Query(min_length=5,max_length=15)) -> List[Movie]:
  db = Session()
  result = MovieService(db).get_movies_by_category(category)
  # return JSONResponse(content=[item for item in movies if item ['category'] == category])
  return JSONResponse(status_code=200,content=jsonable_encoder(result))

# @movie_router.post('/movies', tags=['movies'])
# def create_movie(id: int = Body(), title:str = Body(), overview:str = Body(),year:int = Body(),rating:float = Body(),category:str = Body()):
#   movies.append({
#     "id": id,
#     "title": title,
#     "overview": overview,
#     "year": year,
#     "rating": rating,
#     "category": category
#   })
#   return movies

@movie_router.post('/movies', tags=['movies'])
def create_movie(movie:Movie) -> dict:
  db = Session()
  MovieService(db).create_movie(movie)
  return JSONResponse(content={"Message": "Se ha registrado la pelicula"} )


@movie_router.put('/movies/{id}',tags=['movies'] ,response_model = List[Movie],status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
  db = Session()
  result = MovieService(db).get_movie(id)
  if not result:
      return JSONResponse(status_code=404, content={'message':'No encontrado'})
  
  MovieService(db).update_movie(id, movie)
  return JSONResponse(status_code=200, content={'message':'Se ha modificado la película'})

  # for item in movies:
  #   if item["id"] == id:
  #     item["title"] = movie.title,
  #     item["overview"] = movie.overview,
  #     item["year"] = movie.year,
  #     item["rating"] = movie.rating,
  #     item["category"] = movie.category
    
@movie_router.delete('/movies/{id}',tags=['movies'], response_model = List[Movie],status_code=200)
def delete_movie(id: int) -> dict:
  db = Session()
  result = db.query(MovieModel).filter(MovieModel.id == id).first()
  if not result:
     return JSONResponse(status_code=404,content={'message':'No encontrado'})
  MovieService(db).delete_movie(id)
  return JSONResponse(status_code=202, content={'message' : 'Se ha eliminado la pelicula'})



#OBTENER TODAS LAS COMPUTADORAS
@pc_router.get('/pc', tags=['pc'], response_model= List[Pc], status_code=200)
def get_pc_all() -> List[Pc]:
    db = Session()
    result = db.query(PcModel).all()
    return JSONResponse(status_code=200, content= jsonable_encoder(result))

#OBTENER COMPUTADORAS POR ID
@pc_router.get('/pc/{id}', tags=['pc'])
def get_pcid(id: int = Path(ge=1, le=2000)) -> Pc:
    db = Session()
    result = db.query(PcModel).filter(PcModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': 'No encontrado'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#OBTENER COMPUTADORAS POR MARCA
@pc_router.get('/pc/', tags=['pc'])
def get_by_marca(marca: str = Query(min_length=1, max_length=15)) -> List[Pc]:
    db = Session()
    result = db.query(PcModel).filter(PcModel.marca == marca).all()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#NUEVA COMPUTADORA
@pc_router.post('/pc', tags=['pc'])
def create_pc(pcs: Pc) -> dict:
    db = Session()
    new_pc = PcModel(**pcs.model_dump())
    db.add(new_pc)
    #Guardamos Datos
    db.commit()
    return JSONResponse(content={"message": "Se ha registrado la computadora"})

#ACTUALIZAR COMPUTADORA
@pc_router.put('/pc/{id}', tags=['pc'], response_model= dict, status_code=200)
def update_pc(id: int, pc: Pc) -> dict:
    db = Session()
    result = db.query(PcModel).filter(PcModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'mesaage': 'No encontrada'})
    result.marca = pc.marca
    result.modelo = pc.modelo
    result.color = pc.color
    result.RAM = pc.RAM
    result.almacenamiento = pc.almacenamiento
    db.commit()
    return JSONResponse(status_code=200, content={'message':'Se ha modificado la computadora'})

#ELIMINAR COMPUTADORA POR ID
@pc_router.delete('/pc/{id}', tags=['pc'], response_model= dict, status_code=200)
def delete_pc(id: int) -> dict:
    db = Session()
    result = db.query(PcModel).filter(PcModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'mesaage': 'No encontrado'})
    db.delete(result)
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha eliminado la computadora"})